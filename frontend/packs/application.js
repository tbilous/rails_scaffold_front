import I18n from "i18n-js";
import "components/page/page";
import "components/header/header";
import "bootstrap/dist/js/bootstrap";
import "./application.css";

import translations from "./translations";
import "./utils.coffee";

I18n.locale = document.documentElement.lang; // or window.locale
I18n.translations = translations;
