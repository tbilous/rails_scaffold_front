import Turbolinks from "turbolinks";
import Rails from "rails-ujs";

Turbolinks.start();
Rails.start();

document.addEventListener("turbolinks:load", () => {
  console.log("Turbolinks loaded!");
});
