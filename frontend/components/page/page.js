import { setCallback } from "client/page";
import "./page.css";

const content = document.querySelector(".page-wrapper");

// Telling `chat.js` to call this piece of code whenever a new message is received
// over Action Cable
setCallback(message => {
  content.insertAdjacentHTML("beforeend", message);
});
