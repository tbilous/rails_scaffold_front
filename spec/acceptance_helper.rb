require 'rails_helper'
require 'capybara/email/rspec'
require 'i18n'
require 'selenium/webdriver'
require 'rack_session_access/capybara'

RSpec.configure do |config|
  include ActionView::RecordIdentifier
  config.include AcceptanceHelper, type: :feature
  config.include FeatureMacros, type: :feature

  Capybara.server_host = 'lvh.me'
  Capybara.default_host = "http://#{Capybara.server_host}"
  Capybara.server_port = 4100 + ENV['TEST_ENV_NUMBER'].to_i
  Capybara.default_max_wait_time = 2
  Capybara.save_path = './tmp/capybara_output'
  Capybara.always_include_port = true # for correct app_host
  Capybara.raise_server_errors = false

  chrome_bin_path = ENV.fetch('GOOGLE_CHROME_SHIM', nil)
  headless = 'headless'
  disable_gpu = 'disable-gpu'
  window_size = '--window-size=1300,768'
  options = { args: [window_size, disable_gpu, headless] }
  options[:binary] = chrome_bin_path if chrome_bin_path # only use custom path on heroku
  Capybara.register_driver :headless_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: options
    )

    Capybara::Selenium::Driver.new app,
                                   browser: :chrome,
                                   desired_capabilities: capabilities
  end

  Capybara.javascript_driver = :headless_chrome

  config.use_transactional_fixtures = false

  config.before(:suite) { DatabaseCleaner.clean_with :truncation }

  config.before(:each) { DatabaseCleaner.strategy = :transaction }

  config.before(:each, js: true) { DatabaseCleaner.strategy = :truncation }

  config.before(:each) { DatabaseCleaner.start }

  config.append_after(:each) do
    DatabaseCleaner.clean
  end
end
