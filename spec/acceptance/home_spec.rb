require 'acceptance_helper'

feature 'admin can login', %q{
  Open home page
} do

  context 'visit to root', js: true do
    scenario do
      visit root_path
      expect(page).to have_content 'Pages#home'
    end
  end
end
