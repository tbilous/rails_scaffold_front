module FeatureMacros
  # def sign_out
  #   click_on t('sign_out')
  # end
  #
  # def mail_confirmation(email)
  #   fill_in 'email', with: email
  #   click_button 'submit'
  # end

  def visit_user(user)
    sign_in(user)
    visit root_path
  end

  def visit_quest
    visit root_path
  end
end
