def jwt_token(subject)
  JwtService.encode(payload: { sub: subject.in_app_token })
end
