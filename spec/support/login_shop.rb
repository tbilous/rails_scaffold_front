def login_shop(shop)
  @request.session[:shopify] = shop.id
  @request.session[:shopify_domain] = shop.shopify_domain
end
