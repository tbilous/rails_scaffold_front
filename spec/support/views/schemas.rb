POST_BOXES_SCHEMA = Dry::Validation.Schema do
  schema do
    required(:id).filled(:int?)
    required(:barcode).filled(:str?)
    required(:height).filled(:str?)
    required(:width).filled(:str?)
    required(:depth).filled(:str?)
    required(:weight).filled(:str?)
    required(:comment)
    required(:box_type)
    required(:photo_1_url)
    required(:photo_2_url)
    required(:photo_3_url)
    required(:ip)
    required(:latitude)
    required(:longitude)
  end
end
