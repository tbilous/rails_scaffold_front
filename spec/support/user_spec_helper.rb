module UserSpecHelper
  def user_measurements_one_value(value)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_chestb)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_chestd)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_hip)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_waist)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_arm)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_inseam)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_weight)
    create(:measurement, value: value, user_profile: user_profile, measurement_type: measurement_type_height)
  end

  def size_ranges_for_type_testing(value_min, value_max)
    create(:size_range, measurement_type: measurement_type_chestc, min_value: value_min, max_value: value_max, size: size)
    create(:size_range, measurement_type: measurement_type_hipc, min_value: value_min, max_value: value_max, size: size)
    create(:size_range, measurement_type: measurement_type_waist, min_value: value_min, max_value: value_max, size: size)
    create(:size_range, measurement_type: measurement_type_neckc, min_value: value_min, max_value: value_max, size: size)
    create(:size_range, measurement_type: measurement_type_sleeve, min_value: value_min, max_value: value_max, size: size)
  end

  def user_measurements_one_value_changed(general_value, value, name, is_bmi_30)
    measurement_name_arr = ['chestb', 'chestd', 'hip', 'waist', 'arm', 'inseam']
    temp_arr = []
    measurement_name_arr.each do |m|
      if m == name
        temp_arr.push(value)
      else
        temp_arr.push(general_value)
      end
    end
    puts '^^^^^^^^^'
    puts temp_arr.to_s
    create(:measurement, value: temp_arr[0], user_profile: user_profile, measurement_type: measurement_type_chestb)
    create(:measurement, value: temp_arr[1], user_profile: user_profile, measurement_type: measurement_type_chestd)
    create(:measurement, value: temp_arr[2], user_profile: user_profile, measurement_type: measurement_type_hip)
    create(:measurement, value: temp_arr[3], user_profile: user_profile, measurement_type: measurement_type_waist)
    create(:measurement, value: temp_arr[4], user_profile: user_profile, measurement_type: measurement_type_arm)
    create(:measurement, value: temp_arr[5], user_profile: user_profile, measurement_type: measurement_type_inseam)
    create(:measurement, value: is_bmi_30 ? 90 : 85, user_profile: user_profile, measurement_type: measurement_type_weight)
    create(:measurement, value: 170, user_profile: user_profile, measurement_type: measurement_type_height)
  end
end