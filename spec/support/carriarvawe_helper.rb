# Fog.mock!
# Fog.credentials_path = Rails.root.join('config/application.yml')
# connection = Fog::Storage.new(:provider => 'AWS')
# connection.directories.create(:key => 'my_bucket')

RSpec.configure do |config|
  config.after(:each) do
    FileUtils.rm_rf(Dir["#{Rails.root}/public/spec/uploads"]) if Rails.env.test? || Rails.env.cucumber?
  end
end

if defined?(CarrierWave)
  CarrierWave::Uploader::Base.descendants.each do |klass|
    next if klass.anonymous?

    klass.class_eval do
      def cache_dir
        "#{Rails.root}/spec/support/uploads/tmp"
      end

      def store_dir
        "#{Rails.root}/public/spec/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end
end
