class PageChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'page'
  end
end
